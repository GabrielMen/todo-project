#! /usr/bin/env python3

from flask import Flask, render_template
app = Flask(__name__)
app.debug = True


Gtodo={"Adrien":["Python","Manger","Git"],
       "Audran":["Php","Dormir"],
       "Gabriel":["Java","Manger"]}

@app.route('/')
def home():
    return render_template(
        "home.html")

@app.route('/user/')
@app.route('/user/<name>')
def user(name=None):
    if name in Gtodo.keys():
        todo=Gtodo[name]
    else:
        todo=["rien", "pas grand chose", "sieste"]
    return render_template(
        "user.html",
        name=name,
        todo=todo)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
